package com.studentsecurity.studentsecurity.repository;

import com.studentsecurity.studentsecurity.entity.Student;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import java.util.List;


@Repository
public interface StudentRepository extends JpaRepository<Student, Long>  {

    List<Student> findBystudentId(int student_id);

    List<Student> deleteBystudentId(int student_id);


}