package com.studentsecurity.studentsecurity;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class StudentsecurityApplication {

	public static void main(String[] args) {
		SpringApplication.run(StudentsecurityApplication.class, args);
	}

}
