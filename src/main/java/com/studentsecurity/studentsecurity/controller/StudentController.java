package com.studentsecurity.studentsecurity.controller;


import com.studentsecurity.studentsecurity.entity.Student;
import com.studentsecurity.studentsecurity.repository.StudentRepository;
import jakarta.transaction.Transactional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;


@RestController
@RequestMapping("/school")
public class StudentController {
    @Autowired
    private StudentRepository studentRepository;

    @PostMapping("/addstudent")

    public Student addStudents (Student student){
        return studentRepository.save(student);
    }

    @GetMapping("/students")
    public @ResponseBody Iterable<Student> getAllStudents(){
        return studentRepository.findAll();
    }

    @GetMapping("/student")
    public ResponseEntity<List<Student>> getById (@RequestParam int student_id){
        return new ResponseEntity<>(studentRepository.findBystudentId(student_id), HttpStatus.OK);
    }

    @PutMapping("/update/{student_id}")
    public ResponseEntity<Student> updateTask(@PathVariable int student_id,@RequestBody Student studentDetails ){
        Student student = studentRepository.getById((long) student_id);
        student.setEmail(studentDetails.getEmail());
        student.setFirstName(studentDetails.getFirstName());
        student.setLastName(studentDetails.getLastName());
        student.setCourse(studentDetails.getCourse());
        return new ResponseEntity<>(studentRepository.save(student), HttpStatus.OK);
    }



    @DeleteMapping("/delete/{student_id}")
    @Transactional
    public ResponseEntity<List<Student>> deleteStudent(@PathVariable int student_id){
        return new ResponseEntity<>(studentRepository.deleteBystudentId(student_id),HttpStatus.OK);
    }
}
